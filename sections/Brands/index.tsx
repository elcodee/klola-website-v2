import { Brand } from "@/types/brand";
import Image from "next/image";
import brandsData from "./brandsData";
import SectionTitle from "@/components/SectionTitle";
import Link from "next/link";

const Brands = () => {
  return (
    <section className="pt-16">
      <div className="container">
        <div className="-mx-4 flex flex-wrap">
          {/* <SectionTitle
              title=""
              paragraph="Klola telah membantu meningkatkan efisiensi dan efektifitas pengelolaan SDM di berbagai perusahaan di berbagai industri dan berbagai ukuran dan kondisi kepegawaian di Indonesia"
              center
            /> */}
          <div className="wow fadeInUp w-full mx-auto px-8 text-center max-w-[980px]">
            <h4 className="mb-4 text-lg text-center font-thin !leading-tight text-black dark:text-white sm:text-xl md:text-[18px]">
              {`Klola telah membantu meningkatkan efisiensi dan efektifitas pengelolaan SDM di berbagai perusahaan di berbagai industri dan berbagai ukuran dan kondisi kepegawaian di Indonesia`}
            </h4>
          </div>

          <div className="w-full -mt-10 px-4">
            <div
              className="wow fadeInUp flex flex-wrap items-center justify-center rounded-sm px-8 py-8 sm:px-10 md:px-[50px] md:py-[40px] xl:p-[50px] 2xl:px-[70px] 2xl:py-[60px]"
              data-wow-delay=".1s"
            >
              {brandsData.map((brand) => (
                <SingleBrand key={brand.id} brand={brand} />
              ))}
            </div>
          </div>

          <div className="wow fadeInUp w-full mx-auto text-center max-w-[980px]">
            <h4 className="mb-4 text-base text-center font-bold !leading-tight text-black dark:text-white sm:text-xl md:text-[20px]">
              {`Tertarik dengan Produk kami ? Uji coba sekarang Gratis, atau undang kami untuk demo`}
            </h4>
          </div>
        </div>

        <div className="flex mb-16 flex-col items-center justify-center space-y-4 sm:flex-row sm:space-x-4 sm:space-y-0">
          <Link
            href="#"
            className="rounded-lg bg-primary px-6 py-2 text-base font-semibold text-white duration-300 ease-in-out hover:bg-primary/80"
          >
            Permintaan Demo
          </Link>
          {/* <Link
                    href="https://github.com/Seyma44"
                    className="inline-block rounded-sm bg-black px-8 py-4 text-base font-semibold text-white duration-300 ease-in-out hover:bg-black/90 dark:bg-white/10 dark:text-white dark:hover:bg-white/5"
                  >
                    Explore Demos
                  </Link> */}
        </div>
      </div>
    </section>
  );
};

export default Brands;

const SingleBrand = ({ brand }: { brand: Brand }) => {
  const { href, image, name } = brand;

  return (
    <div className="mx-3 flex w-full max-w-[160px] items-center justify-center py-[15px] sm:mx-4 lg:max-w-[130px] xl:mx-6 xl:max-w-[150px] 2xl:mx-8 2xl:max-w-[160px]">
      <a
        href={href}
        target="_blank"
        rel="nofollow noreferrer"
        className="relative mb-5 w-full opacity-70 grayscale transition hover:opacity-100 hover:grayscale-0 dark:opacity-60 dark:hover:opacity-100"
      >
        <Image src={image} height={100} width={100} alt={name} />
      </a>
    </div>
  );
};
