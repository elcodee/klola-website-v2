import { Brand } from "@/types/brand";

const brandsData: Brand[] = [
  {
    id: 1,
    name: "Gumroad",
    href: "#",
    image: "/clients/dvl.png",
  },
  {
    id: 2,
    name: "Hbomax",
    href: "#",
    image: "/clients/mrt.png",
  },
  {
    id: 3,
    name: "Loom",
    href: "#",
    image: "/clients/g4s.png",
  },
  {
    id: 4,
    name: "Ayro UI",
    href: "#",
    image: "/clients/asco.png",
  },
  {
    id: 5,
    name: "Webflow",
    href: "#",
    image: "/clients/fx-plaza.png",
  },
  {
    id: 6,
    name: "Webflow",
    href: "#",
    image: "/clients/bakmigm.png",
  },
  {
    id: 7,
    name: "Webflow",
    href: "#",
    image: "/clients/inalum.png",
  },
  {
    id: 8,
    name: "Webflow",
    href: "#",
    image: "/clients/cinovasi.png",
  },
  {
    id: 9,
    name: "Webflow",
    href: "#",
    image: "/clients/gelenium.png",
  },
  {
    id: 10,
    name: "Webflow",
    href: "#",
    image: "/clients/etana.png",
  },
  {
    id: 11,
    name: "Webflow",
    href: "#",
    image: "/clients/medi.png",
  },
  {
    id: 12,
    name: "Webflow",
    href: "#",
    image: "/clients/astraindo.png",
  },
  {
    id: 13,
    name: "Webflow",
    href: "#",
    image: "/clients/reswara.png",
  },
  {
    id: 14,
    name: "Webflow",
    href: "#",
    image: "/clients/dayadimensi.png",
  },
];

export default brandsData;
