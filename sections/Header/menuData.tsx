import { Menu } from "@/types/menu";

const menuData: Menu[] = [
  {
    id: 1,
    title: "Beranda",
    path: "/",
    newTab: false,
  },
  // {
  //   id: 2,
  //   title: "Produk",
  //   path: "/about",
  //   newTab: false,
  // },
  {
    id: 2,
    title: "Produk",
    newTab: false,
    submenu: [
      {
        id: 61,
        title: "Klola Cloud",
        desc: "Dengan teknologi cloud, Klola membantu pengelolaan SDM Anda",
        path: "/",
        newTab: false,
      },
      {
        id: 62,
        title: "Payrol Outsourcing",
        desc: "Pengelolaan ekstra administrasi payroll (PPh21) yang mudah, cepat dan akurat",
        path: "/",
        newTab: false,
      },
    ],
  },
  {
    id: 3,
    title: "Perusahaan",
    newTab: false,
    submenu: [
      {
        id: 61,
        title: "Tentang Klola",
        path: "/",
        newTab: false,
      },
      {
        id: 62,
        title: "Liputan Media",
        path: "/",
        newTab: false,
      },
      {
        id: 63,
        title: "Klien",
        path: "/",
        newTab: false,
      },
      {
        id: 64,
        title: "Mitra",
        path: "/",
        newTab: false,
      },
      {
        id: 65,
        title: "Karir & Magang",
        path: "/",
        newTab: false,
      },
    ],
  },
  // {
  //   id: 3,
  //   title: "Perusahaan",
  //   path: "/pricing",
  //   newTab: false,
  // },
  {
    id: 4,
    title: "Hubungi Kami",
    path: "/contact",
    newTab: false,
  },
  // {
  //   id: 4,
  //   title: "Blog",
  //   path: "/blog",
  //   newTab: false,
  // },
  // {
  //   id: 6,
  //   title: "Forums",
  //   newTab: false,
  //   submenu: [
  //     {
  //       id: 61,
  //       title: "Collob",
  //       path: "/",
  //       newTab: false,
  //     },
  //     {
  //       id: 62,
  //       title: "Design",
  //       path: "/",
  //       newTab: false,
  //     },
  //     {
  //       id: 63,
  //       title: "Webco",
  //       path: "/",
  //       newTab: false,
  //     },
  //   ],
  // },
];
export default menuData;
