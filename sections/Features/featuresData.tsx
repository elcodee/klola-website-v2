import { Feature } from "@/types/feature";

const featuresData: Feature[] = [
  {
    id: 1,
    icon: (
      <svg width="40" height="42" fill="#3173c9" viewBox="0 0 24 24" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" stroke="#3173c9"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <path d="M9,24H1a1,1,0,0,1,0-2H9a1,1,0,0,1,0,2Z M7,20H1a1,1,0,0,1,0-2H7a1,1,0,0,1,0,2Z M5,16H1a1,1,0,0,1,0-2H5a1,1,0,0,1,0,2Z M13,23.955a1,1,0,0,1-.089-2A10,10,0,1,0,2.041,11.09a1,1,0,0,1-1.992-.18A12,12,0,0,1,24,12,11.934,11.934,0,0,1,13.09,23.951C13.06,23.954,13.029,23.955,13,23.955Z M12,6a1,1,0,0,0-1,1v5a1,1,0,0,0,.293.707l3,3a1,1,0,0,0,1.414-1.414L13,11.586V7A1,1,0,0,0,12,6Z"></path></g></svg>
    ),
    title: "Efisiensi Waktu",
    paragraph:
      "Klola membantu meningkatkan efisiensi dan efektifitas pengelolaan administrasi SDM, sehingga perusahaan dapat fokus pada aktivitas-aktivitas yang memiliki nilai tambah lebih.",
  },
  {
    id: 1,
    icon: (
      <><svg width="40" height="42" fill="#3173c9" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 407.467 407.467"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <g> <path d="M403.359,320.228L250.938,164.807l41.063-41.063l14.142,14.142l31.82-31.82L231.897,0l-31.819,31.819l14.142,14.142 L97.547,162.634l-14.142-14.142l-31.819,31.82l106.065,106.066l31.82-31.82l-14.142-14.142l40.254-40.254l152.421,155.422 L403.359,320.228z M221.29,31.819l10.606-10.606l84.854,84.853l-10.607,10.607l-3.535-3.535l0,0l-77.781-77.782l0,0L221.29,31.819z M168.258,254.558l-10.607,10.607l-84.853-84.853l10.606-10.607l3.536,3.536l77.782,77.782L168.258,254.558z M108.153,173.241 L224.826,56.568l56.568,56.569l-40.96,40.96l0,0l-17.677,17.678l-58.035,58.035L108.153,173.241z M240.33,175.414l141.919,144.711 l-14.143,14.142L226.189,189.556L240.33,175.414z"></path> <path d="M180.109,332.467h-156v30h-20v45h200v-45h-24V332.467z M39.109,347.467h126v15h-126V347.467z M189.109,377.467v15h-170v-15 h5h156H189.109z"></path> </g> </g></svg></>
    ),
    title: "Kepatuhan Terhadap Peraturan",
    paragraph:
      "Klola membantu pengelolaan SDM di dalam organisasi mematuhi peraturan pemerintah dan selaras dengan best-practices.",
  },
  {
    id: 1,
    icon: (
      <svg width="44" height="46" fill="#3173c9" viewBox="0 0 32 32" data-name="Layer 1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" stroke="#3173c9" stroke-width="0.16"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><rect height="1" width="12" x="10" y="29"></rect><rect height="1" width="12" x="10" y="2"></rect><rect height="1" width="4" x="9" y="5"></rect><rect height="1" width="4" x="9" y="9"></rect><rect height="1" width="10" x="13" y="12"></rect><rect height="1" width="3" x="9" y="12"></rect><rect height="1" width="10" x="13" y="15"></rect><rect height="1" width="3" x="9" y="15"></rect><rect height="1" width="10" x="13" y="18"></rect><rect height="1" width="3" x="9" y="18"></rect><rect height="1" width="10" x="13" y="21"></rect><rect height="1" width="3" x="9" y="21"></rect><rect height="1" width="10" x="13" y="24"></rect><rect height="1" width="3" x="9" y="24"></rect><rect height="1" transform="translate(9.5 41.5) rotate(-90)" width="20" x="15.5" y="15.5"></rect><path d="M22,2V3h2a1,1,0,0,1,1,1V6h1V4a2,2,0,0,0-2-2Z"></path><rect height="1" transform="translate(-9.5 22.5) rotate(-90)" width="20" x="-3.5" y="15.5"></rect><path d="M10,2V3H8A1,1,0,0,0,7,4V6H6V4A2,2,0,0,1,8,2Z"></path><path d="M22,30V29h2a1,1,0,0,0,1-1V26h1v2a2,2,0,0,1-2,2Z"></path><path d="M10,30V29H8a1,1,0,0,1-1-1V26H6v2a2,2,0,0,0,2,2Z"></path><rect height="5" width="1" x="9" y="5"></rect><rect height="5" width="1" x="12" y="5"></rect></g></svg>
    ),
    title: "Informasi Komprehensif",
    paragraph:
      "Klola menghasilkan informasi yang komprehensif mengenai pengelolaan SDM di perusahaan. Dengan dukungan teknologi, kami ingin menyediakan kepada pengguna kami informasi yang komprehensif, berkualitas dan memiliki nilai-tambah bagi pengelolaan SDM di dalam organisasi.",
  },
  {
    id: 1,
    icon: (
      <svg  width="44" height="46" viewBox="0 0 24 24" id="team" xmlns="http://www.w3.org/2000/svg" fill="#000000"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <g id="_24x24_user--dark" data-name="24x24/user--dark"> <rect id="Rectangle" width="24" height="24" fill="none"></rect> </g> <path id="Combined_Shape" data-name="Combined Shape" d="M0,12.106C0,8.323,4.5,9.08,4.5,7.567a2.237,2.237,0,0,0-.41-1.513A3.5,3.5,0,0,1,3,3.4,3.222,3.222,0,0,1,6,0,3.222,3.222,0,0,1,9,3.4,3.44,3.44,0,0,1,7.895,6.053,2.333,2.333,0,0,0,7.5,7.567c0,1.513,4.5.757,4.5,4.54,0,0-1.195.894-6,.894S0,12.106,0,12.106Z" transform="translate(6 8)" fill="none" stroke="#3173c9" stroke-miterlimit="10" stroke-width="1.5"></path> <path id="Combined_Shape-2" data-name="Combined Shape" d="M4.486,12.967c-.569-.026-1.071-.065-1.512-.114A6.835,6.835,0,0,1,0,12.106C0,8.323,4.5,9.08,4.5,7.567a2.237,2.237,0,0,0-.41-1.513A3.5,3.5,0,0,1,3,3.4,3.222,3.222,0,0,1,6,0,3.222,3.222,0,0,1,9,3.4" transform="translate(1 3)" fill="none" stroke="#3173c9" stroke-miterlimit="10" stroke-width="1.5"></path> <path id="Combined_Shape-3" data-name="Combined Shape" d="M-4.486,12.967c.569-.026,1.071-.065,1.512-.114A6.835,6.835,0,0,0,0,12.106C0,8.323-4.5,9.08-4.5,7.567a2.237,2.237,0,0,1,.41-1.513A3.5,3.5,0,0,0-3,3.4,3.222,3.222,0,0,0-6,0,3.222,3.222,0,0,0-9,3.4" transform="translate(23 3)" fill="none" stroke="#3173c9" stroke-miterlimit="10" stroke-width="1.5"></path> </g></svg>
    ),
    title: "Dukungan",
    paragraph:
      "Klola didukung oleh tim yang memiliki pengalaman mendalam dalam membangun, mengimplementasikan dan menangani berbagai kebutuhan organisasi terkait pengelolaan SDM. ",
  }
];
export default featuresData;
