export type Menu = {
  id: number;
  title: string;
  desc?: string;
  path?: string;
  newTab: boolean;
  submenu?: Menu[];
};
