"use client";

import { ThemeProvider } from "next-themes";
import { useEffect } from "react";
import swal from "sweetalert";

export function Providers({ children }: { children: React.ReactNode }) {
  useEffect(() => {
    swal("Important", "This website is under contructions!", "warning", { closeOnClickOutside: false });
  }, [])
  return (
    <ThemeProvider attribute="class" enableSystem={true} defaultTheme="light">
      {children}
    </ThemeProvider>
  );
}
