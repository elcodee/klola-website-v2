import Breadcrumb from "@/components/Breadcrumb";
import Contact from "@/sections/Contact";

import { Metadata } from "next";

export const metadata: Metadata = {
  title: "Contact | Klola Indonesia",
  description: "Klola membantu pengelolaan SDM di dalam organisasi menjadi mudah, cepat dan akurat",
};

const ContactPage = () => {
  return (
    <>
      <Breadcrumb
        pageName="Contact"
      />
      <Contact />
    </>
  );
};

export default ContactPage;
