import AboutSection from "@/sections/About/AboutSection";
import Solutions from "@/sections/About/Solutions";
import Breadcrumb from "@/components/Breadcrumb";

import { Metadata } from "next";

export const metadata: Metadata = {
  title: "About | Klola Indonesia",
  description: "Klola membantu pengelolaan SDM di dalam organisasi menjadi mudah, cepat dan akurat",
};

const AboutPage = () => {
  return (
    <>
      <Breadcrumb
        pageName="About Us"
      />
      <AboutSection />
       <Solutions />
    </>
  );
};

export default AboutPage;
