import AboutSection from "@/sections/About/AboutSection";
import Solutions from "@/sections/About/Solutions";
import Blog from "@/sections/Blog";
import Brands from "@/sections/Brands";
import ScrollUp from "@/components/ScrollToTop/ScrollUp";
import Contact from "@/sections/Contact";
import Features from "@/sections/Features";
import Hero from "@/sections/Hero";
import Pricing from "@/sections/Pricing";
import Testimonials from "@/sections/Testimonials";
import Video from "@/sections/Video";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "Klola Indonesia",
  description: "Klola membantu pengelolaan SDM di dalam organisasi menjadi mudah, cepat dan akurat",
};

export default function Home() {
  return (
    <>
      <ScrollUp />
      <Hero />
      <Features />
      <AboutSection />
      <Solutions />
      <Brands />
      {/* <Pricing /> */}
      {/* <Video /> */}
      {/* <Testimonials /> */}
      {/* <Blog /> */}
      {/* <Contact /> */}
    </>
  );
}
